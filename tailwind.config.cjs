/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ['./src/**/*.{html,js,svelte,ts}'],
  theme: {
    extend: {
        colors: {
            'cool-black': '#1b1c1f',
            'cool-black-hover': '#262a31',
            'heap-overflow-blue': '#1e5cf4'
        }
    },
  },
  plugins: [],
}
