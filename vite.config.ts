import { sveltekit } from '@sveltejs/kit/vite';
import type { UserConfig } from 'vite';
import path from 'path';

const config: UserConfig = {
	resolve:{
		alias:{
		  '$components' : path.resolve(__dirname, './src/components')
		},
	  },
	plugins: [
		sveltekit()
	]
};

export default config;
